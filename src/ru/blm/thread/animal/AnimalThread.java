package ru.blm.thread.animal;

public class AnimalThread extends Thread{
    String name;
    int priority;

    AnimalThread(String n, int p){
        name = n;
        priority = p;
        setName(name);
        setPriority(priority);
    }
    @Override
    public void run() {
        System.out.println("поток запущен");
        for (int i = 0; i < 100; i++) {
            System.out.println(name+" "+i);
            if (i==50){
                if (priority==10) setPriority(1);
                else setPriority(10);
            }
        }
        System.out.println("поток завершен");
    }
}

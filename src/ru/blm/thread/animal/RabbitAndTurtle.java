package ru.blm.thread.animal;

public class RabbitAndTurtle {
    public static void main(String[] args) {
        AnimalThread turtle = new AnimalThread("turtle",1);
        AnimalThread rabbit = new AnimalThread("rabbit",10);

        turtle.start();
        rabbit.start();
    }
}
